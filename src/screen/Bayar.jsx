import React, {useState, useEffect} from 'react';
import { Text, View, TouchableOpacity,  Image, FlatList} from 'react-native';
import Icon  from 'react-native-vector-icons/Ionicons';
import Header from '../source/Header';
import LinearGradient from 'react-native-linear-gradient';


const Bayar = () => {

  const [history, setHistory] = useState([
    {
      name: 'Pembayaran Sepaket',
      description: 'Pembayaran Makanan dan Minuman',
      price: 25000,
      date: '29/12/2023',
      icon: 'fast-food-outline',
    },
    {
      name: 'Pembayaran Ikan ',
      description: 'Pembayaran Ikan Bakar',
      price: 45000,
      date: '29/12/2023',
      icon: 'fish-outline',
    },
    {
      name: 'Pembayaran Jus Alpukat',
      description: 'Pembayaran Minuman Dingin',
      price: 15000,
      date: '29/12/2023',
      icon: 'beer-outline',
    },
  ]);

  return (
    <View style={{flex: 1, backgroundColor: '#F5DEB3'}}>
      <Header />
        <View style={{flex: 1, marginHorizontal: 20, marginTop: 10}}>
          <Text style={{fontWeight: 'bold', fontSize: 18, color: 'black'}}>Welcome FOODUI</Text>
          <Text style={{fontWeight: 'bold', fontSize: 18,color: 'black'}}>My Wallet</Text>
         <TouchableOpacity>
          <LinearGradient 
           start={{x: 0, y: 0}} 
           end={{x: 1, y: 0}}
           style={{borderRadius: 15, marginTop: 20}}
           colors={['#4DA0B0', '#FFF94C', '#D39D38']}>
            
          <View 
            style={{
              padding: 20,
              borderRadius: 10,

          }} >
            
            <Text>Balance</Text>
            <Text style={{fontSize: 24, fontWeight: 'bold', marginTop: 10}}>Rp 100.000.000</Text>
            <View style={{flexDirection: 'row'}}>
            <View style={{marginTop: 60}}>
              <Text>Card Owner</Text>
              <Text>FOODUI</Text>
            </View>
            <View style={{justifyContent: 'flex-end', alignItems: 'flex-end', flex: 1}}>
             <Image 
              source={require('../FOTO/Mastercard.png')} 
              style={{width: 50, height: 50}}
              resizeMode="contain" />
            </View>
            </View>
            
            
        
        </View>   
          </LinearGradient>
          </TouchableOpacity>
          <TouchableOpacity 
            style={{
              pading: 12, 
               
              marginTop: 20, 
              borderRadius: 3,
              borderWidth: 4,
              borderStyle: 'dashed',
              borderColor: '#696969'
              }}>
            <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'black', fontSize: 20}}>+ Add New Card</Text>
          </TouchableOpacity>
          <View style={{marginTop: 20, flex: 1,}}>
            <Text style={{fontWeight: 'bold', color: 'black'}}>History</Text>
            <FlatList 
              data={history} 
              style={{marginBottom: 20}}
              renderItem={({item}) => (
                 <View 
                  style={{
                    marginTop: 5,
                    backgroundColor: '#DCDCDC',
                    elevation: 2,
                    borderRadius: 8,
                    paddingHorizontal: 20,
                    paddingVertical: 20,
                    marginBottom: 5,
                    }}>
                      <View style={{flexDirection: 'row'}}>
                        
                        <View
                         
                          style={{
                            justifyContent: 'center', 
                            alignItems: 'center', 
                            marginRight: 20}}>
                            <TouchableOpacity>
                               <Icon name={item.icon}size ={30} color="#808080" /> 
                            </TouchableOpacity>
                        </View>
                        
                      <View style={{flex: 1}}>
                        <TouchableOpacity>
                        <Text style={{color: 'black'}}>{item.date}</Text>
                        <Text style={{color: 'black', fontSize: 16, fontWeight: 'bold'}}>{item.name}</Text>
                        <Text style={{color: 'black'}}>{item.description}</Text>
                        <Text style={{color: 'black', textAlign: 'right'}}>Rp
                        {item.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}</Text>
                        </TouchableOpacity>
                      </View>
                      </View>
                      
                      
                     
                </View>
            )} 
            />
              
          </View>
        
      </View>
      
    </View>
  )
}

export default Bayar

