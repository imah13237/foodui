import React, { useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Input from '../component/Input';

const Saya = () => {
  const [nama, setNama] = useState('');
  const [alamat, setAlamat] = useState('');
  const [nope, setNope] = useState('');
  const [saran, setSaran] = useState('');

  return (
    <View style={styles.box}>
      <Text 
      style={{alignItems: 'center', 
      justifyContent: 'center', 
      color: '#8B4513'}}>Haii, Terima Kasih sudah Mengunjugi FOODUI</Text>
      <Text
      style={{alignItems: 'center', 
      justifyContent: 'center', 
      color: '#8B4513'}}>Silahkan Konfirmasi Diri Anda, Bahwa anda Pernah Beintraksi Bersama Kami 
      </Text>
      <Input
        label="Nama"
        placeholder="Input Nama Anda"
        value={nama}
        onChangeText={(text) => setNama(text)}
      />
      <Input
        label="Alamat"
        placeholder="Input Alamat Anda"
        value={alamat}
        onChangeText={(text) => setAlamat(text)}
      />
      <Input
        label="Nope"
        placeholder="Input Nope Anda"
        inputMode="numeric"
        value={nope}
        onChangeText={(text) => setNope(text)}
      />
       <Input
        label="Saran"
        placeholder="Kritik & Saran Kepada Kami"
        value={saran}
        onChangeText={(text) => setSaran(text)}
      />
      <Text style={styles.text}>Nama: {nama}</Text>
      <Text style={styles.text}>Alamat: {alamat}</Text>
      <Text style={styles.text}>Nope: {nope}</Text>
      <Text style={styles.text}>Kritik & Saran: {saran}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  box: {
    padding: 15,
    flex: 1,
    backgroundColor: '#F5DEB3',
  },
  text: {
    color: '#8B4513',
    marginTop: 15,
  },
});

export default Saya;
