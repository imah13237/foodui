import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  StatusBar,
  Image,
  ScrollView,
  ImageBackground,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

const Menu = () => {
  const [kategori, setKategori] = useState([
    {
      nama: 'Makanan',
    },
    
    {
      nama: 'Minuman',
    },
    {
      nama: 'Buah',
    },
    {
      nama: 'Gorengan',
    },
    
  ]);
  const [kategoriSeleksi, setKategoriSeleksi] = useState({
    nama: 'Makanan,Minuman,Buah,Gorengan',
  });

 

  const [dataTrending, setDataTrending] = useState([
    {
      namaResep: ' Jus Alpukat',
      author: 'Ulif',
      image: require('../images/1.jpg'),
    },
    {
      namaResep: 'Bakso lava',
      author: 'Iim',
      image: require('../images/2.jpg'),
    },
    {
      namaResep: 'Dimsum',
      author: 'Dila',
      image: require('../images/3.jpg'),
    },
    {
      namaResep: 'Ikan bakar bumbu kuning',
      author: 'Ulif',
      image: require('../images/4.jpg'),
    },
  ]);

  const [dataVideo, setDataVideo] = useState([
    {
      namaResep: 'Es Teler',
      author: 'Iim',
      image: require('../images/5.jpg'),
      length: '10:10',
    },
    {
      namaResep: 'Nasi Goreng Seafood',
      author: 'Ulif',
      image: require('../images/6.jpg'),
      length: '09:09',
    },
    {
      namaResep: 'Ayam Geprek Special',
      author: 'Dila',
      image: require('../images/7.jpg'),
      length: '12:13',
    },
    {
      namaResep: 'Sop Iga Sapi',
      author: 'Ulif',
      image: require('../images/8.jpg'),
      length: '12:14',
    },
  ]);

  return (
    <View style={{flex: 1, backgroundColor: '#F5DEB3'}}>
      <ScrollView>
        <StatusBar backgroundColor="#f5f5f5" barStyle="dark-content" />
        <View style={{
          marginHorizontal: 20,
         marginBottom: 20,
          marginTop: 20}}
        onPress={() => setKategori(item)}>
       
          <Text style={{fontSize: 28, fontWeight: 'bold', color: '#212121'}}>
            MENU<Text style={{color: '#4169e1'}}>FOODUI</Text>
          </Text>
        </View>
        <View>
          <FlatList
            data={kategori}
            horizontal
            showsHorizontalScrollIndicator={false}
            style={{marginLeft: 10}}
            renderItem={({item}) => (
              <TouchableOpacity
                style={{
                  marginRight: 5,
                  backgroundColor:
                    kategoriSeleksi.nama == item.nama ? '#4169e1' : '#fff',
                  elevation: 3,
                  paddingHorizontal: 15,
                  paddingVertical: 8,
                  marginBottom: 10,
                  borderRadius: 15,
                  marginLeft: 5,
                }}
                onPress={() => setKategoriSeleksi(item)}>
                <Text
                  style={{
                    color:
                      kategoriSeleksi.nama == item.nama ? '#fff' : '#212121',
                  }}>
                  {item.nama}
                </Text>
              </TouchableOpacity>
            )}
          />
        </View>

        <View
          style={{
            marginHorizontal: 20,
            marginBottom: 10,
            marginTop: 20,
            flexDirection: 'row',
          }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => setDataTrending(item)}>
            <Text style={{fontSize: 18, fontWeight: 'bold', color: '#212121'}}>
              Trending
            </Text>
          </View>

          <TouchableOpacity
            style={{
              justifyContent: 'flex-end',
              alignItems: 'center',
              flex: 1,
              flexDirection: 'row',
              marginTop: 10,
            }}>
            <Text style={{fontSize: 14}}>Lihat Semua</Text>
            <Icon name="chevron-forward" size={20} color="#bdbdbd" />
          </TouchableOpacity>
        </View>

        <View>
          <FlatList
            data={dataTrending}
            horizontal
            showsHorizontalScrollIndicator={false}
            style={{marginLeft: 10}}
            renderItem={({item}) => (
              <TouchableOpacity
                style={{
                  marginRight: 5,
                  backgroundColor: '#fff',
                  elevation: 3,
                  paddingHorizontal: 15,
                  paddingVertical: 8,
                  marginBottom: 10,
                  borderRadius: 15,
                  marginLeft: 5,
                }}>
                <Image
                  source={item.image}
                  style={{
                    width: 200,
                    height: 150,
                    marginTop: 10,
                    marginBottom: 10,
                    borderRadius: 3,
                  }}
                  resizeMode={'stretch'}
                />
                <Text
                  style={{
                    color: '#212121',
                    fontSize: 18,
                    fontWeight: 'bold',
                  }}>
                  {item.namaResep}
                </Text>
                <Text>{item.author}</Text>
              </TouchableOpacity>
            )}
          />
        </View>

        <View
          style={{
            marginHorizontal: 20,
            marginBottom: 10,
            marginTop: 20,
            flexDirection: 'row',
          }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => setDataVideo(item)}>
            <Text style={{fontSize: 18, fontWeight: 'bold', color: '#212121'}}>
              Video Masak
            </Text>
          </View>

          <TouchableOpacity
            style={{
              justifyContent: 'flex-end',
              alignItems: 'center',
              flex: 1,
              flexDirection: 'row',
              marginTop: 10,
            }}>
            <Text style={{fontSize: 14}}>Lihat Semua</Text>
            <Icon name="chevron-forward" size={20} color="#bdbdbd" />
          </TouchableOpacity>
        </View>

        <View>
          <FlatList
            data={dataVideo}
            horizontal
            showsHorizontalScrollIndicator={false}
            style={{marginLeft: 10}}
            renderItem={({item}) => (
              <TouchableOpacity
                style={{
                  marginRight: 5,
                  backgroundColor: '#fff',
                  elevation: 3,
                  paddingHorizontal: 15,
                  paddingVertical: 8,
                  marginBottom: 10,
                  borderRadius: 15,
                  marginLeft: 5,
                }}>
                <ImageBackground
                  source={item.image}
                  style={{
                    width: 200,
                    height: 150,
                    marginTop: 10,
                    marginBottom: 10,
                    borderRadius: 3,
                  }}
                  resizeMode={'stretch'}>
                  <View style={{flex: 1}}>
                    <View style={{flex: 1}}></View>
                    <View
                      style={{
                        flexDirection: 'row',
                      }}>
                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                          backgroundColor: 'rgba(0,0,0,0.7)',
                          paddingTop: 2,
                          paddingBottom: 2,
                        }}>
                        <Icon
                          style={{marginLeft: 5}}
                          name="play-circle"
                          size={15}
                          color="#bdbdbd"
                        />
                      </View>
                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                          backgroundColor: 'rgba(0,0,0,0.7)',
                          paddingRight: 10,
                          paddingTop: 2,
                          paddingBottom: 2,
                          paddingLeft: 4,
                        }}>
                        <Text style={{color: '#FFFFFF'}}>{item.length}</Text>
                      </View>
                    </View>
                  </View>
                </ImageBackground>
                <Text
                  style={{
                    color: '#212121',
                    fontSize: 18,
                    fontWeight: 'bold',
                  }}>
                  {item.namaResep}
                </Text>
                <Text>{item.author}</Text>
              </TouchableOpacity>
            )}
          />
        </View>
      </ScrollView>
      
      
    </View>
  );
};

export default Menu;
