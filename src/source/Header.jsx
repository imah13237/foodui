import React from 'react';
import { Text, View, TouchableOpacity, StatusBar } from 'react-native';
import Icon  from 'react-native-vector-icons/FontAwesome';

const Header = () => {
  return (
      <View 
        style={{
          flexDirection: 'row', 
          paddingVertical: 10,
          backgroundColor: '#F8F8FF',
          }}>
       
        <TouchableOpacity 
           style={{
            justifyContent: 'center', 
            alignItems: 'center', 
            paddingHorizontal: 20,
            }}>
        <Icon name="bars" size ={30} color="#808080" />
      </TouchableOpacity>
      <StatusBar   barStyle='light-content'/>
      <View
         style={{flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          paddingHorizontal: 10,
          }} >
         <Text style={{fontSize: 18, fontWeight: 'bold', color: 'black'}}>FOODUI Wallet</Text>
      </View>
        <TouchableOpacity
          style={{
            justifyContent: 'center', 
            alignItems: 'center', 
            paddingHorizontal: 20,
            }}>
        <Icon name="bell" size={30} color="#808080" />
      </TouchableOpacity>

      </View>

  )
}

export default Header

