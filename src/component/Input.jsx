import React, { useState } from 'react';
import { View, StyleSheet, TextInput } from 'react-native';

const Input = ({ label, placeholder, inputMode, onChangeText, value }) => {
  return (
    <View style={styles.kotakInput}>
      <TextInput
        placeholder={placeholder}
        value={value}
        onChangeText={onChangeText}
        inputMode={inputMode}
      />
    </View>
  );
};

export default Input;

const styles = StyleSheet.create({
  kotakInput: {
    borderWidth: 1,
    paddingHorizontal: 5,
    marginBottom: 10,
    borderRadius: 10,
    borderColor: 'gray',
    backgroundColor: '#DAA520',
  },
});