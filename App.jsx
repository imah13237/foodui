import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StatusBar } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Beranda from './src/screen/Beranda';
import Bayar from './src/screen/Bayar';
import Saya from './src/screen/Saya';
import Menu from './src/screen/Menu';




const App = () => {
  const [activeMenu, setActiveMenu] = useState('Beranda');
  const Tab = createBottomTabNavigator();

  return (
    <View style={{ flex: 1, backgroundColor: '#FAFAFA' }}>
     
      {activeMenu == 'Menu' && <Menu />}
      {activeMenu == 'Bayar' && <Bayar />}
      {activeMenu == 'Beranda' && <Beranda />}
      {activeMenu == 'Saya' && <Saya />}      

      <View
        style={{
          backgroundColor: '#FFFFFF',
          flexDirection: 'row',
          paddingVertical: 12,
          borderTopWidth: 0,
          borderRadius: 9,
          borderTopColor: '#89F06',
          justifyContent: 'space-around',
          alignItems: 'center',
          bottom: 0,
          left: 0, 
          right: 0, 
        }}>
        <TouchableOpacity
          onPress={() => setActiveMenu('Beranda')}
          style={{ flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: activeMenu == 'Beranda' ? '#F5DEB3' : '#FFFFFF',
            elevation: activeMenu == 'Beranda' ? 2 : 0,
            paddingVertical: 12,
            borderRadius: 9,
          }}>
          <Icon name="home" size={22} color="#CD853F" />
          <Text style={{ color: '#CD853F' }}>Beranda</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => setActiveMenu('Menu')}
          style={{ flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 12,
            borderRadius: 9,
            backgroundColor: activeMenu == 'Menu' ? '#F5DEB3' : '#FFFFFF',
            elevation: activeMenu == 'Menu' ? 2 : 0,
          }}>
          <Icon name="utensils" size={22} color="#CD853F" />

          <Text style={{ color: activeMenu == 'Menu' ? '#CD853F' : '#CD853F' }}>
            Menu
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => setActiveMenu('Bayar')}
          style={{ flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: activeMenu == 'Bayar' ? '#F5DEB3' : '#FFFFFF',
            elevation: activeMenu == 'Bayar' ? 2 : 0,
            paddingVertical: 12,
            borderRadius: 9,
          }}>
          <Icon name="credit-card" size={22} color="#CD853F" />
          <Text style={{ color: activeMenu == 'Bayar' ? '#CD853F' : '#CD853F' }}>
           Bayar
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => setActiveMenu('Saya')}
          style={{ flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: activeMenu == 'Saya' ? '#F5DEB3' : '#FFFFFF',
            elevation: activeMenu == 'Saya' ? 2 : 0,
            paddingVertical: 12,
            borderRadius: 9,
          }}>
          <Icon name="user" size={22} color="#CD853F" />
          <Text style={{ color: activeMenu == 'Saya' ? '#CD853F' : '#CD853F' }}>
            Saya
          </Text>
        </TouchableOpacity>
      </View>
      <StatusBar backgroundColor={'#FAFAFA'} barStyle="dark-content" />
    </View>
  );
};

export default App;

